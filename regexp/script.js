// 1.Написать регулярное выражение, которое найдет в тексте все совпадения и выведет на
// экран.

const str = "Lorem ipsum dolor sit amet, consectetur vue adipisicing elit. Nihil ipsa Vue sunt, voluptas odititaque deleniti vUe voluptatum cumque accusamus. Impedit, doloremque."

let regexp = /vue/gi;

let result = str.match(regexp)
console.log(result)

// 2. Реализовать валидацию Email и если Email написан верно, возвращать true иначе
// false. Важно, чтобы Email соответствовал структуре: первый символ обязательно буква,
// дальше могут быть и буквы и числа, потом собачка @, потом доменное имя, потом точка и
// сам домен.

function isValidEmail(myPhone) { 
    return /^[A-Z._%+-]+@[A-Z0-9-]+.+.[A-Z]{2,4}$/i.test(myPhone); 
 } 

let email1 = "example@text.com";
let email2 = "4554example@text.com";
let email3 = "example@com";
let email4 = "example-test.com";

console.log(isValidEmail(email4))
