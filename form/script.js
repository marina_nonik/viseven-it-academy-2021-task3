const form = document.getElementById("dev-form")

// console.log(form)
// const position = document.getElementsByName("position")
// console.log(position)

const inputs = document.querySelectorAll("input, textarea");

// console.log(inputs)

const getcheckboxes = (name) => {
    return Array.from(form.querySelectorAll("input[name='${name}']"))
    .filter(checkbox => checkbox.checked)
    .map(checkbox => checkbox.value);
}

form.addEventListener("submit", (e) => {
    e.preventDefault();
    
    const formObject = Array.from(inputs).reduce((obj, field) => {
       obj[field.name] = field.type === "checkbox" ? getcheckboxes(field.name) : field.value;

       return obj;
    }, {});

    console.log(formObject);
    console.log(JSON.stringify(formObject));
});
